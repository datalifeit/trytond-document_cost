# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from itertools import groupby
from decimal import Decimal
from simpleeval import simple_eval
from trytond.model import fields, DeactivableMixin
from trytond.tools import decistmt
from trytond.pyson import Eval, Id, Bool, Not, If
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext
from sql import Null
from sql.conditionals import Coalesce
from sql.functions import Trim

DISTRIBUTION_METHODS = [
    ('untaxed_amount', 'Untaxed amount'),
    ('total_amount', 'Total amount'),
    ('quantity', 'Quantity')
]


def cost_mixin(distribution_methods=DISTRIBUTION_METHODS):

    class CostMixin(object):
        """Cost Mixin"""
        __slots__ = ()

        formula = fields.Char('Formula')
        distribution_method = fields.Selection(distribution_methods,
            'Distribution method', required=True)

        @classmethod
        def __register__(cls, module_name):
            super().__register__(module_name)
            table = cls.__table_handler__(module_name)
            if table._columns['formula']['notnull']:
                table.not_null_action('formula', action='remove')
                # change formula 0 by empty only the first time
                cls._update_required_formula()

        @classmethod
        def _update_required_formula(cls):
            pass

        @classmethod
        def default_distribution_method(cls):
            return distribution_methods[0][0]

        def check_formula(self):
            if self.formula:
                context = self.get_context_formula(None)

                try:
                    value = self.get_amount(**context)
                    if not isinstance(value, Decimal) and not isinstance(
                            Decimal(value), Decimal):
                        raise ValueError
                except ValueError as exception:
                    raise UserError(gettext(
                        'document_cost.msg_document_cost_invalid_formula',
                        formula=self.formula,
                        cost=self.rec_name,
                        exception=exception))

        def get_context_formula(self, cost):
            return {
                'names': {m[0]: Decimal(
                    getattr(cost, 'document_%s' % m[0], 0) or '0.0')
                    for m in distribution_methods}
                }

        def get_amount(self, **context):
            """Return amount (as Decimal)"""
            context.setdefault('functions', {})['Decimal'] = Decimal
            return simple_eval(decistmt(self.formula), **context)

    return CostMixin


def cost_type(distribution_methods=DISTRIBUTION_METHODS):

    class CostTypeMixin(DeactivableMixin, cost_mixin(distribution_methods)):
        """Cost Type Mixin"""
        __slots__ = ()

        name = fields.Char('Name', required=True, translate=True,
            states={'readonly': ~Eval('active')})
        product = fields.Many2One('product.product', 'Product',
            domain=[
                ('type', '=', 'service'),
                ('default_uom', '=', Id('product', 'uom_unit')),
                ('template.type', '=', 'service'),
                ('template.default_uom', '=', Id('product', 'uom_unit'))],
            states={
                'readonly': ~Eval('active'),
                'required': (Eval('apply_method') != 'none'),
                'invisible': (Eval('apply_method') == 'none')
            })
        apply_method = fields.Selection([
            ('none', 'None')], 'Apply method',
            states={'readonly': ~Eval('active')})
        manual = fields.Boolean('Manual',
            states={'readonly': ~Eval('active')},
            help='The cost type can be added manually to a Document.')
        quantity_formula = fields.Char('Quantity Formula', required=True,
            states={'readonly': ~Eval('active')})

        @classmethod
        def __setup__(cls):
            super(CostTypeMixin, cls).__setup__()
            for _field_name in ('formula', 'distribution_method'):
                _field = getattr(cls, _field_name, None)
                if _field.states.get('readonly'):
                    _field.states['readonly'] |= ~Eval('active')
                else:
                    _field.states['readonly'] = ~Eval('active')

        @classmethod
        def __register__(cls, module_name):
            super().__register__(module_name)

            table_h = cls.__table_handler__(module_name)
            table_h.not_null_action('product', action='remove')

        @classmethod
        def default_apply_method(cls):
            return 'none'

        @staticmethod
        def default_quantity_formula():
            return '1'

        @classmethod
        def validate(cls, lines):
            super(CostTypeMixin, cls).validate(lines)
            for line in lines:
                line.check_formula()
                line.check_quantity_formula()

        def check_quantity_formula(self):
            context = self.get_context_formula(None)

            try:
                value = self.get_quantity(**context)
                if not isinstance(value, float) and not isinstance(
                        float(value), float):
                    raise ValueError
            except ValueError as exception:
                raise UserError(gettext(
                    'document_cost.msg_document_cost_invalid_qty_formula',
                    formula=self.formula,
                    cost=self.rec_name,
                    exception=exception))

        def get_quantity(self, **context):
            """Return quantity (as float)"""
            context.setdefault('functions', {})['float'] = float
            qty = float(simple_eval(self.quantity_formula, **context))
            if self.product:
                qty = self.product.default_uom.round(qty)
            return qty

    return CostTypeMixin


def cost_document(distribution_methods=DISTRIBUTION_METHODS):

    _fields = [m[0] for m in distribution_methods]

    class CostDocumentMixin(cost_mixin(distribution_methods)):
        """Cost Document Mixin"""
        __slots__ = ()

        amount = fields.Numeric('Amount',
            digits=(16, Eval('currency_digits', 2)))
        apply_method = fields.Function(
            fields.Selection([('none', 'None')], 'Apply method'),
            'on_change_with_apply_method', searcher='search_apply_method')
        currency = fields.Function(
            fields.Many2One('currency.currency', 'Currency'),
            'get_currency')
        currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')
        quantity = fields.Function(
            fields.Float('Quantity', digits=(16, Eval('unit_digits', 2))),
            'get_quantity')
        unit_digits = fields.Function(
            fields.Integer('Unit digits'), 'get_unit_digits')
        compute_formula = fields.Function(fields.Boolean('Compute formula'),
            'get_compute_formula')

        if 'untaxed_amount' in _fields:
            document_untaxed_amount = fields.Function(
                fields.Numeric('Document untaxed amount',
                    digits=(16, Eval('currency_digits', 2))),
                'get_document_untaxed_amount')

            @fields.depends('document',
                '_parent_document.untaxed_amount_precost_cache',
                '_parent_document.untaxed_amount', '_parent_document.lines',
                methods=['_must_check_lines', '_valid_line_for_cost'])
            def get_document_untaxed_amount(self, name=None):
                if self.document:
                    if not self._must_check_lines():
                        if getattr(self.document,
                                'untaxed_amount_precost_cache'):
                            return self.document.untaxed_amount_precost_cache
                        return self.document.untaxed_amount or 0
                    else:
                        return sum(line.amount or 0
                            for line in self.document.lines
                            if self._valid_line_for_cost(line))
                return 0

        if 'total_amount' in _fields:
            document_total_amount = fields.Function(
                fields.Numeric('Document total amount',
                    digits=(16, Eval('currency_digits', 2))),
                'get_document_total_amount')

            @fields.depends('document',
                '_parent_document.total_amount_precost_cache',
                '_parent_document.total_amount', '_parent_document.lines',
                methods=['_must_check_lines', '_valid_line_for_cost'])
            def get_document_total_amount(self, name=None):
                pool = Pool()
                Tax = pool.get('account.tax')

                if self.document:
                    if not self._must_check_lines():
                        if getattr(self.document,
                                'total_amount_precost_cache'):
                            return self.document.total_amount_precost_cache
                        return self.document.total_amount
                    else:
                        lines = [line for line in self.document.lines
                            if self._valid_line_for_cost(line)]
                        total_amount = sum(line.amount for line in lines)
                        for line in lines:
                            l_taxes = Tax.compute(line.taxes, line.unit_price,
                                line.quantity, self.document.tax_date)
                            for tax in l_taxes:
                                taxline = self.document._compute_tax_line(**tax)
                                total_amount += taxline['amount']
                        return total_amount
                return 0

        if 'quantity' in _fields:
            document_quantity = fields.Function(
                fields.Float('Document quantity',
                    digits=(16, Eval('document_unit_digits', 2))),
                'get_document_quantity')
            document_unit_digits = fields.Function(
                fields.Integer('Unit Digits'),
                'on_change_with_document_unit_digits')

            @classmethod
            def get_document_quantity(cls, records, name=None):
                raise NotImplementedError()

            @fields.depends('document')
            def on_change_with_document_unit_digits(self, name=None):
                if self.document and not isinstance(self.document, tuple):
                    lines = [l for l in self.document.lines
                        if not l.cost and l.type == 'line']
                    base_uom = CostDocumentMixin._get_base_uom(lines)
                    if base_uom:
                        return base_uom.digits
                return 2

        @classmethod
        def _update_required_formula(cls):
            sql_table = cls.__table__()
            cursor = Transaction().connection.cursor()
            cursor.execute(*sql_table.update(
                [sql_table.formula],
                [Null],
                where=(
                    (Trim(sql_table.formula) == '0')
                    & (Coalesce(sql_table.amount, 0) != 0)))
            )

        @fields.depends(*(['type_', '_parent_type_.apply_method',
            '_parent_type_.distribution_method', '_parent_type_.formula']
            + ['document_%s' % f for f in _fields]),
                methods=['on_change_formula'])
        def on_change_type_(self):
            if not self.type_:
                return
            self.apply_method = self.type_.apply_method
            self.distribution_method = self.type_.distribution_method
            self.formula = self.type_.formula
            self.on_change_formula()

        @fields.depends('formula', 'currency', 'document', methods=[
            '_compute_document_values', 'must_compute_formula', 'compute'])
        def on_change_formula(self):
            if not self.formula or not self.must_compute_formula():
                return

            self._compute_document_values()
            _amount = self.compute()

            if self.currency:
                _amount = self.currency.round(_amount)
            self.amount = _amount

        @fields.depends(methods=['get_compute_formula'])
        def must_compute_formula(self):
            return self.get_compute_formula()

        def get_compute_formula(self, name=None):
            return bool(self.formula is not None)

        def compute(self, context=None):
            """Compute cost amount"""
            if not context:
                context = self.get_context_formula(self)
            return self.get_amount(**context)

        def get_currency(self, name=None):
            if self.document and self.document.currency:
                return self.document.currency.id
            return None

        def get_currency_digits(self, name=None):
            if self.currency:
                self.currency.digits
            return 2

        def get_quantity(self, name=None):
            context = self.get_context_formula(self)
            return self.type_.get_quantity(**context)

        def get_unit_digits(self, name=None):
            return (self.type_.product and
                self.type_.product.default_uom.digits or 2)

        @classmethod
        def _get_base_uom(cls, lines):
            pool = Pool()
            Uom = pool.get('product.uom')

            uomcat_ids = set(l.unit.category.id for l in lines)
            if not len(uomcat_ids) == 1:
                return None
            uomcat_ids, = uomcat_ids
            base_uom, = Uom.search([
                ('category', '=', uomcat_ids),
                ('rate', '=', 1),
                ('factor', '=', 1)], limit=1)
            return base_uom

        @classmethod
        def apply(cls, records):
            # reload records
            records = cls.browse(list(map(int, records)))
            if records:
                records.sort(key=lambda cost: cost.apply_method)
                for apply_method, costs in groupby(
                        records, key=lambda cost: cost.apply_method):
                    cls._apply_method(apply_method, list(costs))

        @classmethod
        def unapply(cls, records):
            if records:
                records.sort(key=lambda cost: cost.apply_method)
                for apply_method, costs in groupby(
                        records, lambda cost: cost.apply_method):
                    cls._unapply_method(apply_method, list(costs))

        @fields.depends('type_')
        def on_change_with_apply_method(self, name=None):
            if self.type_:
                return self.type_.apply_method
            return None

        @classmethod
        def search_apply_method(cls, name, clause):
            return [('type_.apply_method', ) + tuple(clause[1:])]

        @classmethod
        def _apply_method(cls, apply_method, costs):
            pass

        @classmethod
        def _unapply_method(cls, apply_method, costs):
            pass

        @fields.depends('document')
        def _compute_document_values(self):
            pass

        def _must_check_lines(self):
            return False

        def _valid_line_for_cost(self, line):
            return True

        @property
        def amount_formula_computable(self):
            dist_methods = [
                s[0] for s in self.__class__.distribution_method.selection
                if s[0] != 'none']
            return any(dm in self.formula for dm in dist_methods)

        @property
        def quantity_formula_computable(self):
            dist_methods = [
                s[0] for s in self.__class__.distribution_method.selection
                if s[0] != 'none']
            return any(dm in self.type_.quantity_formula
                    for dm in dist_methods)

    return CostDocumentMixin


def cost_document_line(distribution_methods=DISTRIBUTION_METHODS):

    _fields = [m[0] for m in distribution_methods]

    class CostDocumentLineMixin(object):
        """Cost Document line Mixin"""
        __slots__ = ()

        amount = fields.Numeric('Amount',
            digits=(16, Eval('currency_digits', 2)))
        document_date = fields.Function(
            fields.Date('Document date'), 'get_document_date',
            searcher='search_document_data')
        apply_method = fields.Function(
            fields.Selection([('none', 'None')], 'Apply method'),
            'get_apply_method', searcher='search_apply_method')
        currency = fields.Function(
            fields.Many2One('currency.currency', 'Currency'),
            'get_currency')
        currency_digits = fields.Function(fields.Integer('Currency Digits'),
            'get_currency_digits')
        _document_attr = ''
        _document_date = 'date'

        if 'untaxed_amount' in _fields:
            document_untaxed_amount = fields.Function(
                fields.Numeric('Document untaxed amount',
                    digits=(16, Eval('currency_digits', 2))),
                'get_document_untaxed_amount')

            def get_document_untaxed_amount(self, name=None):
                if self.document_line:
                    return self.document_line.amount
                return 0

        if 'total_amount' in _fields:
            document_total_amount = fields.Function(
                fields.Numeric('Document total amount',
                    digits=(16, Eval('currency_digits', 2))),
                'get_document_total_amount')

            @classmethod
            def get_document_total_amount(cls, records, name=None):
                pool = Pool()
                Tax = pool.get('account.tax')
                Date = pool.get('ir.date')

                values = {r.id: r.document_line.amount for r in records}
                document_line_taxes = {i: 0 for i in list(set(
                    [r.document_line for r in records if r.document_line]))}
                for document_line, _ in document_line_taxes.items():
                    tax_date = getattr(
                        getattr(document_line, cls._document_attr),
                        'taxes_date', Date.today())
                    l_taxes = Tax.compute(document_line.taxes,
                        document_line.unit_price, document_line.quantity,
                        tax_date)
                    for tax in l_taxes:
                        taxline = getattr(document_line,
                            cls._document_attr)._compute_tax_line(**tax)
                        document_line_taxes[document_line] += taxline['amount']

                for record in records:
                    if not record.document_line:
                        continue
                    values[record.id] += document_line_taxes[
                        record.document_line]

                return values

        if 'quantity' in _fields:
            document_quantity = fields.Function(
                fields.Float('Document quantity',
                    digits=(16, Eval('document_unit_digits', 2))),
                'get_document_quantity')
            document_unit_digits = fields.Function(
                fields.Integer('Unit Digits'),
                'on_change_with_document_unit_digits')

            @classmethod
            def get_document_quantity(cls, records, name=None):
                pool = Pool()
                Uom = pool.get('product.uom')

                values = {r.id: 0 for r in records}
                uomcat_ids = [r.document_line.unit.category.id for r in records]
                uoms = Uom.search([('category', 'in', uomcat_ids),
                                   ('rate', '=', 1),
                                   ('factor', '=', 1)])
                base_uoms = {}
                for uom in uoms:
                    if uom.category.id not in base_uoms:
                        base_uoms.setdefault(uom.category.id, uom)
                for record in records:
                    qty = Uom.compute_qty(record.document_line.unit,
                        record.document_line.quantity,
                        base_uoms[record.document_line.unit.category.id])
                    values[record.id] = qty
                return values

            @fields.depends('document_line', '_parent_document_line.id')
            def on_change_with_document_unit_digits(self, name=None):
                if self.document_line:
                    return self.document_line.digits
                return 2

        @classmethod
        def search_rec_name(cls, name, clause):
            return ['OR',
                    ('cost', ) + tuple(clause[1:]),
                    ('document_line', ) + tuple(clause[1:])]

        @classmethod
        def search_document_data(cls, name, clause):
            field = clause[0]
            if name == 'document_date':
                field = field.replace(name, cls._document_date, 1)
            s_string = 'document_line.%s.%s' % (cls._document_attr, field)
            return [(s_string, ) + tuple(clause[1:])]

        @property
        def document(self):
            return getattr(self.document_line,
                CostDocumentLineMixin._document_attr)

        @classmethod
        def get_document_date(cls, records, name=None):
            return {r.id: getattr(r.document, cls._document_date)
                for r in records}

        @classmethod
        def get_apply_method(cls, records, name=None):
            return {r.id: r.cost.apply_method for r in records}

        @classmethod
        def search_apply_method(cls, name, clause):
            return [('cost.apply_method', ) + tuple(clause[1:])]

        def get_currency(self, name=None):
            if self.document and self.document.currency:
                return self.document.currency.id
            return None

        def get_currency_digits(self, name=None):
            if self.currency:
                return self.currency.digits
            return 2

        @fields.depends('document_line', '_parent_document_line.id')
        def on_change_document_line(self):
            if not self.document_line:
                return
            self._compute_document_values()

        def _compute_document_values(self):
            if 'untaxed_amount' in _fields:
                self.document_untaxed_amount = self.get_document_untaxed_amount()
            if 'total_amount' in _fields:
                self.document_total_amount = self.get_document_total_amount(
                    [self])[self.id]
            if 'quantity' in _fields:
                self.document_quantity = self.get_document_quantity(
                    [self])[self.id]

        @classmethod
        def delete(cls, records):
            with Transaction().set_context(_check_access=False):
                super(CostDocumentLineMixin, cls).delete(records)

        def get_context_formula(self):
            return {
                'names': {m[0]: Decimal(
                    getattr(self, 'document_%s' % m[0], 0) or '0.0')
                    for m in distribution_methods}
                }

        def get_quantity(self):
            """Return quantity (as float)"""
            context = self.get_context_formula()
            context.setdefault('functions', {})['float'] = float
            qty = float(simple_eval(self.cost.type_.quantity_formula,
                **context))
            return qty

    return CostDocumentLineMixin


class AccountMixin(object):
    __slots__ = ()

    account = fields.Many2One('account.account', 'Account',
        domain=[('company', '=', Eval('context', {}).get('company', -1))],
        states={
            'required': (~Eval('product')
                & (Eval('apply_method') != 'none')),
            'invisible': (
                (Eval('apply_method') == 'none') | Bool(Eval('product')))
        })

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.product.states['required'] &= Not(Eval('account'))
        cls.product.states['invisible'] |= Bool(Eval('account'))

        def get_if_statement(conditions):
            if not conditions or not conditions[0][0]:
                return []
            current = conditions[0]
            if len(conditions) > 1:
                others = conditions[1:]
            else:
                others = []
            return If(Eval('apply_method') == current[0], current[1],
                get_if_statement(others))

        cls.account.domain.append(get_if_statement(cls._account_domain()))

    @classmethod
    def _account_domain(cls):
        return [(None, [])]

    def on_change_account(self):
        self.product = None

    def on_change_product(self):
        self.account = None

    @property
    def account_used(self):
        return self.__class__(self.id).account
