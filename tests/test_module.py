# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class DocumentCostTestCase(ModuleTestCase):
    """Test Document Cost module"""
    module = 'document_cost'


del ModuleTestCase
